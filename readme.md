# SWS Sandbox Template

The SWS Sandbox Template is a template constructed for the specific use of the SWS CMS selection sandboxing activity. This template is not for production use, nor should it be considered a comprehensive example of compliance with Department of Education best practices and code standards and guidelines.


## Distributable/built code

All built code (HTML, CSS, JS etc) is located in the "_dist_" directory. Any modifications made in this directory will be lost whenever the source files are compiled. Modification to the template code must be made in the "src" directory.

### Template preview (v1.0.0)

A built copy of the SWS Sandbox Template version 1.0.0 can be viewed on http://dec-ce.bitbucket.org/sws/sandbox/1/

## Source

### Prerequisites
Modifying the template code requires the use and knowledge of:
* [Node.js](https://nodejs.org/en/) & [npm](https://www.npmjs.com)/[Yarn](https://www.npmjs.com) - Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine. npm and Yarn are node.js package managers.
* [Gulp](http://gulpjs.com) - build workflow automation tool
* [Bower](https://bower.io) - web development library/code package manager
* [UIKit](http://getuikit.com) - web UI framework similar to Bootstrap
* [Pug](https://pugjs.org/api/getting-started.html) - template engine and language
* [SASS](http://sass-lang.com) - CSS extension language that compiles to CSS
* [ECMAScript 2015 (6th edition) a.k.a. ES6](https://babeljs.io/docs/learn-es2015/) - a contemporary version of the ECMAScript/JavaScript standard supported by newer browsers

## Source directory structure

All source files (.pug, .scss, .js) are located in the "_src_" directory. The Gulp build workflows are highly dependent on the folder structure within _src_. Please take some time to learn the folder structure and learn to work within it. Do not modify the structure unless you simultaneously modify the Gulp config (_gulpfile.js_)

## Building the source

Assuming you have have correctly installed the prerequisite npm and Bower modules, to compile the source files (Pug, SASS, ES6 etc) into vanilla HTML, CSS, JS etc you can execute the following Gulp tasks:

### Build

Compiles source files into distributable/built vanilla HTML, CSS, JS code. All compiled and optimised assets are stored in the "_dist_" directory.

    $ gulp build

### Watch

To continually build source files whenever a change is made:

    $ gulp watch

or simply

    $ gulp

The _watch_ task will also run a temporary node.js based web server accessible at http://localhost:3000/ allowing you to preview changes. Additionally it uses [Browsersync](https://www.browsersync.io) to automatically refresh the browser window whenever the page is updated.

### Deploy

Gulp can be used to build and deploy distributable/built code to the Digital Services test server (http://dec-ce.bitbucket.org/). Executing the _deploy_ task requires access to the private [dec-ce.bitbucket.org Bitbucket Git repository](https://bitbucket.org/dec-ce/dec-ce.bitbucket.org) and for it to be checked out as sibling directory to the _sws-sandbox-template_ directory.

    $ gulp deploy --env=test


### Contributing to the Git repo

The Digital Services team utilizes the  [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) branching model and pull requests to manage contributions from multiple contributors. Additionally feature branch names are named after their corresponding JIRA ticket code e.g. SPWS-XXX

## Maintainers
* Alex Motyka [alex.motyka@det.nsw.edu.au](mailto:alex.motyka@det.nsw.edu.au)
* Simon Law [simon.law2@det.nsw.edu.au](simon.law2@det.nsw.edu.au)
