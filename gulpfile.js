"use strict"

// Load required libraries
var gulp            = require("gulp"),
    runSequence     = require("run-sequence"),
    argv            = require("yargs").argv,
    pug             = require("pug"),
    plugins         = require("gulp-load-plugins")(),
    browserSync     = require("browser-sync").create(),
    git             = require("gulp-git"),
    util            = require("util"),
    errorHandler    = function (error) {
      console.log(error.toString())
      this.emit('end')
    }

// Define options and paths
var options = {
  compress:               argv.compress || false,
  port:                   argv.port || 3000,
  paths: {
    bower:                "bower_components/",
    dev: {
      root:               "src/",
      templates:          "src/*.pug",
      assets: {
        root:             "src/assets/",
        sass:             "src/assets/sass/**/*.{scss,sass}",
        js:               "src/assets/js/**/*.{js,es6,es7,es}",
        fonts:            "src/assets/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}",
        images:           "src/assets/images/**/*.{gif,jpg,svg,png}",
        pug:              "src/assets/pug/**/*.pug"
      }
    },
    build: {
      root:               "dist/",
      templates:          "dist/*.html",
      assets: {
        root:             "dist/assets/",
        css:              "dist/assets/css/",
        js:               "dist/assets/js/",
        fonts:            "dist/assets/fonts/",
        images:           "dist/assets/images/"
      }
    }
  },
  deployment: {
    source:               "dist/**/*.*",
    environment:          undefined,
    repository: {
      root:               "../dec-ce.bitbucket.org/",
      deploymentLocation: "../dec-ce.bitbucket.org/sws/sandbox/"
    },
    args: {
      args:               "",
      cwd:                "../dec-ce.bitbucket.org/",
      quiet:              false
    }
  }
}


// Delete everything in the "build" folder
gulp.task("clean", require("./tasks/clean")(gulp, plugins, options))

// Copy bower libraries
gulp.task("copy", require("./tasks/copy")(gulp, plugins, options, errorHandler))

// Copy fonts found in the project's source files
gulp.task("fonts", require("./tasks/fonts")(gulp, plugins, options, errorHandler))

// Copy images (svg, gif, png, jpg) found in the project's source files
gulp.task("images", require("./tasks/images")(gulp, plugins, options, errorHandler))

// Git repository short hash
gulp.task("revision", require("./tasks/revision")(gulp, plugins, options, errorHandler))

// pug templating engine
gulp.task("pug", ["revision"], require("./tasks/pug")(gulp, plugins, options, errorHandler, pug))

// Compile CSS (SASS)
gulp.task("compile-css", require("./tasks/compile-css")(gulp, plugins, options, errorHandler, browserSync))

// convert ECMAScript 6 & 7 code to ECMAScript 5 code
gulp.task("babel", require("./tasks/babel")(gulp, plugins, options, errorHandler))

// Web server and livereload
gulp.task("browser-sync", require("./tasks/browser-sync")(gulp, browserSync, options, errorHandler))



// Build Task - Compiles all template and code from the src into the dist folder
gulp.task("build", function(callback) {

  return runSequence("build-copy", ["build-pug", "build-css", "build-js"], callback)
})



// ------------------------ //
//    DEVELOPMENT TASKS     //
// ------------------------ //

// Build Copy - All the required copy tasks in order
gulp.task("build-copy", function(callback) {

  return runSequence("clean", ["copy", "images", "fonts"], callback)
})

// Build pug - All the required pug tasks in order
gulp.task("build-pug", function() {

  gulp.start("pug")
})

// Build CSS - All the required pug tasks in order
gulp.task("build-css", function() {

  gulp.start("compile-css")
})

// Build JS - All the required JS tasks in order
gulp.task("build-js", function(callback) {

  return runSequence("babel", callback)
})

// Build Task - Compiles all template and code from the src into the dist folder
gulp.task("build", function(callback) {

  return runSequence("build-copy", ["build-pug", "build-css", "build-js"], callback)
})

// Start task - runs Browser Sync so things can be served
gulp.task("start", ["build"], function() {
  return gulp.start("browser-sync")
});

// Watch task. Uses BrowserSync to provide a local development web server with livereload capability e.g.
//
// $ gulp watch
//
// You can optionally pass the "compress" CLI parameter to serve compressed assets e.g.
//
// $ gulp watch --compress
//
gulp.task("watch", ["start"], function() {

  // Process tasks
  gulp.watch(options.paths.dev.assets.sass, ["compile-css"])
  gulp.watch(options.paths.dev.assets.js, ["babel"])
  gulp.watch(options.paths.dev.assets.fonts, ["fonts"])
  gulp.watch(options.paths.dev.assets.images, ["images"])
  gulp.watch(options.paths.dev.templates, ["pug"])

  // Watch for JS and HTML
  gulp.watch([options.paths.build.assets.js + "**/*.js", options.paths.build.templates], browserSync.reload)

})


// ------------------------ //
//    DEPLOYMENT TASKS      //
// ------------------------ //

// Pull first to catch any updates
gulp.task("git-pull", function(callback){
  return git.pull('origin', 'master', {cwd: options.deployment.repository.root}, function (err) {
      if (err) {
        throw err
      } else {
        runSequence('git-deploy', callback)
      }
  });
});

// Copy the files
gulp.task("git-cp", function() {
  return gulp.src(options.deployment.source)
    .pipe(gulp.dest(options.deployment.repository.deploymentLocation))
})

// Add any new files to the repo
gulp.task("git-add", ["git-cp"], function() {
  return gulp.src("./.", {cwd: options.deployment.repository.root})
    .pipe(git.add({cwd: options.deployment.repository.root}, function(err) {
      if (err) {
        throw err
      } else {
        console.log('Added new files!');
      }
    }))
});

// Automated commit message
gulp.task("git-commit", ["git-add"], function() {
  return gulp.src("./", {cwd: options.deployment.repository.root})
    .pipe(git.commit("Automated deployment to SWS Sandbox Temaplate " + options.deployment.environment + " repository", {cwd: options.deployment.repository.root}, function(err) {
        this.emit("end")
        console.log("The " + options.deployment.environment + " environment is already up to date. Nothing to be added :D")
    })
  )
})

// Push that code.
gulp.task("git-deploy", ["git-commit"], function(callback) {
    return git.push("origin", "master", options.deployment.args, function(err) {
      if (err) {
        throw err
      } else {
        console.log("You've successfully updated the SWS Sandbox Temaplate " + options.deployment.environment + " environment!")
        gulp.src(__filename)
        .pipe(plugins.open({uri: "https://dec-ce.bitbucket.com/sws/sandbox/" + options.deployment.environment + "/"}))
      }
    })
})

// Automated Deployment
// To deploy to dec-ce.bitbucket.org/gef/(string) run the "deploy" task and pass a string of your choice (default: undefined)
//
// $ gulp deploy --env=review
//
//
gulp.task("deploy", function(callback) {

  options.compress = false

  // Set the environment if CLI parameter is set
  options.deployment.environment = argv.env !== undefined ? argv.env : options.deployment.environment

  if (options.deployment.environment !== undefined) {
    // Assign the new deployment location
    options.deployment.repository.deploymentLocation += options.deployment.environment
    // Run the Deployment
    runSequence("build", "git-pull", callback)
  } else {
    console.log("Please supply a deployment environment. e.g. $ gulp deploy --env=SPWS-782")
  }
})

// Default run task
gulp.task("default", function(callback) {
  gulp.start("watch", callback)
})
