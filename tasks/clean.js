"use strict"

module.exports = function(gulp, plugins, options) {
  return function() {
    return gulp.src(options.paths.build.root, { read: false })
      .pipe(plugins.clean())
  }
}
