"use strict"

module.exports = function(gulp, plugins, options) {
  return function(callback) {
    plugins.git.revParse({args:"--short HEAD"}, function (error, hash) {
      options.revision = hash;
      callback(error);
    });
  };
};
