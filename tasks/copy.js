"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {

    // jQuery
    gulp.src(options.paths.bower + "jquery/dist/" + (options.compress ? "jquery.min.js" : "jquery.js"))
      .pipe(plugins.rename("jquery.js"))
      .on("error", errorHandler)
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor"))

    // UIKit
    var uikitPaths
    if (options.compress) {
      uikitPaths = [options.paths.bower + "uikit/js/**/*.min.js"]
    } else {
      uikitPaths = [options.paths.bower + "uikit/js/**/*.js", "!" + options.paths.bower + "uikit/js/**/*.min.js"]
    }

    gulp.src(uikitPaths)
      .pipe(plugins.rename(function(path) {
        path.basename = path.basename.replace(".min", "")
      }))
      .on("error", errorHandler)
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor/uikit"))

    gulp.src(options.paths.bower + "uikit/fonts/**/*.{tff,woff,woff2,otf}")
      .on("error", errorHandler)
      .pipe(gulp.dest(options.paths.build.assets.fonts))

    // Lorem Ipsum mixin (moving this out of bower components into src for use in pug)
    gulp.src(options.paths.bower +  "lorem-ipsum-mixin/mixin-lorem-ipsum.jade")
      .pipe(plugins.rename("mixin-lorem-ipsum.pug"))
      .pipe(gulp.dest(options.paths.dev.root + "assets/pug"))

  }
}
