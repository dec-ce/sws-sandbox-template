"use strict"

module.exports = function(gulp, plugins, options, errorHandler, browserSync) {
  return function() {
    return gulp.src(options.paths.dev.assets.sass)     // Convert SASS to CSS
      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      .pipe(plugins.sass({
        includePaths: "bower_components",
        outputStyle: options.compress ? "compressed" : "expanded"
      }))

      // Append any missing vendor prefixes for CSS3 properties
      .pipe(plugins.autoprefixer({
          browsers: ["last 2 versions", "IE 9", "IE 10"],
          cascade: false
      }))

      // If the --compress flag is true then compress the CSS
      // don't run autoprefixer because it strips out important ie9 stuff
      .pipe(plugins.if(options.compress, plugins.cssnano( {autoprefixer: false} )))

      // Write the CSS files to disk
      .pipe(gulp.dest(options.paths.build.assets.css))

      // Inject CSS
      .pipe(browserSync.stream())
  }
}
