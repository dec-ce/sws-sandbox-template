"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {
    // Images (svg, png, gif, jpg) found in src/assets/images
    return gulp.src(options.paths.dev.assets.images)
      .pipe(gulp.dest(options.paths.build.assets.images))
      .on("error", errorHandler)
  }
}
